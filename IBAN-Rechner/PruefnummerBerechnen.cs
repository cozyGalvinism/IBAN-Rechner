﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			PruefnummerBerechnen.cs
Einsatz:		Berechnet die Prüfnummer anhand der Kontonummer, Bankleitzahl und Länderkennung
Beschreibung:	Kombiniert zuerst Bankleitzahl, Kontonummer und die Zahl der Länderkennung + 00, kovertiert diese zu einer großen Zahl (int[]),
                berechnet den Modulo aus der großen Zahl und 97, zieht diesen von 98 ab und füllt die resultierende Zahl auf 2 Zeichen mit 0.
Funktionen:		PruefnummerBerechnen
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static string PruefnummerBerechnen(string bban, string laenderkennung)
        {
            string kombiniert = bban + LandZuZahl(laenderkennung) + "00";
            int[] kombiniertZahl = StringZuBigIntArray(kombiniert);
            int moduloKombiniert = Modulo(kombiniertZahl, 97);
            string pruefnummer = (98 - moduloKombiniert).ToString("00");
            return pruefnummer;
        }
    }
}
