﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			IBANValidieren.cs
Einsatz:		Validiert eine IBAN
Beschreibung:	Überprüft IBAN auf Länge, überprüft die Länderkennung, ob diese aus Großbuchstaben besteht und überprüft ob die Prüfnummer stimmt
Funktionen:		IBANValidieren
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
10.09.2018      IBAN wird nun zu Beginn auf Länge geprüft
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static bool IBANValidieren(string iban, ref string grund, IBANFormat[] formate)
        {
            if(iban == "")
            {
                grund = "IBAN ist leer!";
                return false;
            }

            if (iban.Length < 2)
            {
                grund = "IBAN zu kurz!";
                return false;
            }
            string laenderkennung = iban.Substring(0, 2);
            bool enthaeltLand = false;
            IBANFormat format = new IBANFormat();
            for (int i = 0; i < formate.Length; i++)
            {
                if (formate[i].GetKuerzel() == laenderkennung)
                {
                    enthaeltLand = true;
                    format = formate[i];
                    break;
                }
            }

            if (!enthaeltLand)
            {
                grund = "Das angegebene Land konnte nicht gefunden werden.";
                return false;
            }

            string landKuerzel = format.GetKuerzel();
            string landFormat = format.GetFormat();

            if (iban.Length != landFormat.Length + 4)
            {
                grund = "IBAN zu kurz oder zu lang!";
                return false;
            }

            string pruefnummer = iban.Substring(2, 2);
            string bban = iban.Substring(4);

            if ((laenderkennung[0] < 65 || laenderkennung[0] > 90) ||
                (laenderkennung[1] < 65 || laenderkennung[1] > 90))
            {
                grund = "Länderkennung sind nicht vollständig Buchstaben!";
                return false;
            }

            string kombiniert = bban + LandZuZahl(laenderkennung) + pruefnummer;
            int[] kombiniertZahl = StringZuBigIntArray(kombiniert);
            int modulo = Modulo(kombiniertZahl, 97);
            if (modulo != 1)
            {
                grund = "Prüfnummer inkorrekt!";
                return false;
            }

            return true;
        }
    }
}
