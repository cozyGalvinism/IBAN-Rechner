﻿/*****************************************************************************
h e i n r i c h -h e r t z -b e r u f s k o l l e g  d e r  s t a d t  b o n n
Autor:			Jean Luc Nürrenberg
Klasse:			IA117
Datum:			30.08.2018
Datei:			Max.cs
Einsatz:		Gibt die größere Zahl aus
Beschreibung:	Vergleicht 2 Zahlen miteinander und gibt die jeweils größere aus (Equivalent zu Math.Max(int, int) )
Funktionen:		Max
******************************************************************************
Aenderungen:
30.08.2018      Erstellung
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static int Max(int zahl1, int zahl2)
        {
            if (zahl1 <= zahl2)
            {
                return zahl2;
            }
            else
            {
                return zahl1;
            }
        }
    }
}
