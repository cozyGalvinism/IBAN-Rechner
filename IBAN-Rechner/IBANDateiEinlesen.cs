﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        static void IBANDateiEinlesen(string pfad, ref IBANFormat[] formate)
        {
            string[] datei = File.ReadAllLines(pfad);
            formate = new IBANFormat[datei.Length];

            for (int i = 0; i < datei.Length; i++)
            {
                string datensatz = datei[i];
                string[] landFormat = datensatz.Split(';');

                string land = landFormat[0];

                string ibanFormat = landFormat[1];
                string ibanKuerzel = ibanFormat.Substring(0, 2);
                string kuerzel = ibanKuerzel;

                string bban = ibanFormat.Substring(4);
                string formatString = bban;

                IBANFormat format = new IBANFormat(land, kuerzel, formatString);
                formate[i] = format;
            }
        }
    }
}
