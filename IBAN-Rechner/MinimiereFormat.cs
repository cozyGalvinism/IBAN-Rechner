﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {

        static string MinimiereFormat(string format)
        {
            format = EntferneDuplikate(format, 'b');
            format = EntferneDuplikate(format, 'd');
            format = EntferneDuplikate(format, 'k');
            format = EntferneDuplikate(format, 'r');
            format = EntferneDuplikate(format, 's');
            format = EntferneDuplikate(format, 'X');
            return format;
        }

        static string EntferneDuplikate(string text, char duplikat)
        {
            int start = 0;
            string ausgabe = "";
            for (int i = 0; i < text.Length - 1; i++)
            {
                if (text[i] == duplikat && text[i + 1] == duplikat)
                {
                    ausgabe = ausgabe + text.Substring(start, i - start);
                    start = i + 1;
                }
            }

            ausgabe = ausgabe + text.Substring(start, text.Length - start);
            return ausgabe;
        }
    }
}
