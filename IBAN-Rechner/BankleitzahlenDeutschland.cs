﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBAN_Rechner
{
    partial class main
    {
        struct DeutscheBankleitzahl
        {
            string bankleitzahl;
            string bezeichnung;
            string postleitzahl;
            string ort;
            string kurzbezeichnung;
            string bic;

            public DeutscheBankleitzahl(string bankleitzahl, string bezeichnung, string postleitzahl, string ort, string kurzbezeichnung, string bic)
            {
                this.bankleitzahl = bankleitzahl;
                this.bezeichnung = bezeichnung;
                this.postleitzahl = postleitzahl;
                this.ort = ort;
                this.kurzbezeichnung = kurzbezeichnung;
                this.bic = bic;
            }

            public string GetBankleitzahl()
            {
                return bankleitzahl;
            }

            public string GetBezeichnung()
            {
                return bezeichnung;
            }

            public string GetPostleitzahl()
            {
                return postleitzahl;
            }

            public string GetOrt()
            {
                return ort;
            }

            public string GetKurzbezeichnung()
            {
                return kurzbezeichnung;
            }

            public string GetBIC()
            {
                return bic;
            }
        }
    }
}
